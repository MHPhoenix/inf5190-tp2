class User:
    def __init__(self, id, nom, courriel, etablissements, passp):
        self.id = id
        self.nom = nom
        self.courriel = courriel
        self.etablissements = etablissements
        self.passp = passp

    def asDictionary(self):
        return {"id": self.id,
                "nom": self.nom,
                "courriel": self.courriel,
                "etablissements": self.etablissements,
                "passp": self.passp}