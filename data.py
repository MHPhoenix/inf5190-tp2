import datetime
import sqlite3


"""
    S'occupe des echanges avec la base de donnees
"""
class Contrevenant():

    """
        Les methodes qui suivent s'inspirent des exemples
        de classe du professeur Jacques Berger
    """
    
    def __init__(self):
        self.connection = None

    def get_connection(self):
        if self.connection is None:
            self.connection = sqlite3.connect('database/db.db')
        return self.connection

    def disconnect(self):
        if self.connection is not None:
            self.connection.close()

    """
        Fonction de recherche de contrevenant
    """
    def recherche(self, a_chercher):

        cursor = self.get_connection().cursor()
        cursor.execute("""SELECT * 
                        FROM contrevenant WHERE proprietaire LIKE ?
                        OR etablissement LIKE ? OR adresse LIKE ?""", ('%'+a_chercher, '%'+a_chercher, '%'+a_chercher+'%'))
        rech = cursor.fetchall()

        if len(rech) == 0:
            return None
        else:
            return [(cherch[0], cherch[1], cherch[2], cherch[3], cherch[4], 
            cherch[5], cherch[6], cherch[7], cherch[8]) for cherch in rech]

    """
        fonction de recherche de contrevenant entre deux dates
    """
    def recherche_entre_date(self, date_debut, date_fin):

        cursor = self.get_connection().cursor()
        cursor.execute(("""SELECT * 
                        FROM contrevenant 
                        WHERE date_infraction
                        BETWEEN ?
                        AND ?"""), (date_debut, date_fin))

        rech = cursor.fetchall()
       
        if len(rech) == 0:
            return None
        else:
            return [(cherch[0], cherch[1], cherch[2], cherch[3], cherch[4], 
            cherch[5], cherch[6], cherch[7], cherch[8]) for cherch in rech]

    """
        fonction de recherche de contrevenant selon le nom
        de l'etablissement entre deux dates et recupere le 
        nombre de contraventions obtenues par celui-ci 
        durant cette periode
    """
    def recherche_par_etab(self, date_debut, date_fin):

        cursor = self.get_connection().cursor()
        cursor.execute(("""SELECT etablissement, COUNT(*) AS nbre_apparition 
                        FROM contrevenant
                        WHERE date_infraction
                        BETWEEN ?
                        AND ?
                        GROUP BY etablissement"""), (date_debut, date_fin))

        rech = cursor.fetchall()
       
        if len(rech) == 0:
            return None
        else:
            
            return [({"Etablissements":cherch[0], "Contraventions":cherch[1]}) for cherch in rech]

    """
        Fonction de recherche de contrevenant par noms
        Cette fonction est appelee pour afficher les
        contrevenant dans une liste deroulante
    """
    def recherche_par_nom(self):

        cursor = self.get_connection().cursor()
        cursor.execute("""SELECT DISTINCT etablissement 
                        FROM contrevenant""")
        rech = cursor.fetchall()

        if len(rech) == 0:
            return None
        else:
            return [(cherch[0]) for cherch in rech]

    """
        Fonction qui affiche la description des infractions
    """
    def info_contravention(self, nom):
        cursor = self.get_connection().cursor()
        cursor.execute("""SELECT descrip, montant 
                        FROM contrevenant
                        WHERE etablissement LIKE ?""", ('%'+nom+'%',))
        rech = cursor.fetchall()

        if len(rech) == 0:
            return None
        else:
            return [(cherch[0], cherch[1]) for cherch in rech]

    """
        fonction qui selectionne la liste de tous les contrevenants,
        avec le nombre d'infractions connues et les retourne
        selon le nombre d'infractions(ordre decroissant)
    """
    def select_etab_decr(self):

        cursor = self.get_connection().cursor()
        cursor.execute(("""SELECT etablissement, COUNT(*) AS nbre_infraction 
                        FROM contrevenant
                        GROUP BY etablissement 
                        ORDER BY nbre_infraction DESC"""))

        rech = cursor.fetchall()
       
        if len(rech) == 0:
            return None
        else:
            
            return [({"Etablissements":cherch[0], "Contraventions":cherch[1]}) for cherch in rech]