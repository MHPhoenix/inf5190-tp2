//Demarrage de la fonction
$(function(){

    //fonction pour A5
    $('a#process_input').bind('click', function(){
        $.getJSON('/api/contrevenants/', {
            du: $('input[name="du"]').val(),
            au: $('input[name="au"]').val(),
        }, function(data) {
            $("#result").append(data.result);
        });
        return false;
    });
});


//fonction pour A6
$(function (){
    $('a#process_input2').bind('click', function(){
        $.getJSON('/api/infos/', {
            nom: document.querySelector('#liste').value
        }, function(data) {
            $("#resultat").append(data.resultat);
        });
        return false;
    });
});


//Fonction pour E1
function sendData() {
    var data = {};
    data.nom = document.getElementById("nom");
    data.courriel = document.getElementById("courriel");
    data.etablissements = document.getElementById("etablissements");
    data.passp = document.getElementById("passp");
    var json = JSON.stringify(data);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/utilisateur/confirmation/", true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8')

    xhr.onload = function() {
        var le_user = JSON.parse(xhr.responseText);
    }

    xhr.send(json);
}