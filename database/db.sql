create table contrevenant (
  proprietaire VARCHAR(100),
  categorie VARCHAR(100),
  etablissement VARCHAR(100),
  adresse VARCHAR(100),
  ville VARCHAR(100),
  descrip VARCHAR(1000),
  date_infraction VARCHAR(30),
  date_jugement VARCHAR(30),
  montant VARCHAR(50)
);