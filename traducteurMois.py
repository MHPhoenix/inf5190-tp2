import datetime

def traducteur_mois(date_a_traduire):

    janvier = "janvier"
    fevrier = "février"
    mars = "mars"
    avril = "avril"
    mai = "mai"
    juin = "juin"
    juillet = "juillet"
    aout = "août"
    septembre = "septembre"
    octobre = "octobre"
    novembre = "novembre"
    decembre = "décembre"

    jan = "Jan"
    feb = "Feb"
    mar = "Mar"
    apr = "Apr"
    may = "May"
    jun = "Jun"
    jul = "Jul"
    aug = "Aug"
    sep = "Sep"
    octb = "Oct"
    nov = "Nov"
    dec = "Dec"

    mois_anglais = [jan, feb, mar, apr, may, jun, jul, aug, sep, octb, nov, dec]

    la_date = date_a_traduire.split()

    mois = [janvier, fevrier, mars, avril, mai, juin, juillet, 
            aout, septembre, octobre, novembre, decembre]

    for le_mois in mois:

        if le_mois in date_a_traduire:
            la_date[1] = mois_anglais[mois.index(le_mois)]
            date_infraction = la_date[0] + " " + la_date[1] + " " + la_date[2]

    date_infraction = datetime.datetime.strptime(date_infraction, '%d %b %Y')

    return date_infraction