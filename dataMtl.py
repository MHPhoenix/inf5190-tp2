import datetime
import sqlite3
import os
import yaml
import yagmail
import tweepy


"""
    S'occupe des fonctions du backgroundScheduler
"""
class ContrevenantMtl():

    """
        Les methodes qui suivent s'inspirent des exemples
        de classe du professeur Jacques Berger
    """

    def __init__(self):
        self.connection = None

    def get_connection(self):
        if self.connection is None:
            self.connection = sqlite3.connect('database/montreal.db')
        return self.connection

    def disconnect(self):
        if self.connection is not None:
            self.connection.close()

    """
        Fonction qui synchronises les donnees de la ville de montreal
    """
    def synchro_database(self):
        os.system('python3 scraperMTL.py')
        liste_nouveaux_contrev = []

        cursor = self.get_connection().cursor()
        #On recupere les donnees de mtl dans montreal.db
        cursor.execute("SELECT * FROM contrevenant")
        mtl_contrevenant = cursor.fetchall()

        #Connection a la base de donnees principale (contrevenant)
        conn = sqlite3.connect('database/db.db')
        cur = conn.cursor()
        cur.execute("SELECT * FROM contrevenant")
        db_contrevenant = cur.fetchall()

        for mtl_con in mtl_contrevenant:
        
            #On insere les donnees recuperees dans la base de donnee principale
            if mtl_con not in db_contrevenant:
                cur.execute(("""INSERT INTO contrevenant(proprietaire, categorie, etablissement, 
                adresse, ville, descrip, date_infraction, date_jugement, montant)"""
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)"),
                (mtl_con[0], mtl_con[1], mtl_con[2], mtl_con[3], mtl_con[4], mtl_con[5], mtl_con[6], mtl_con[7], mtl_con[8]))

                liste_nouveaux_contrev.append(mtl_con[2])
                ContrevenantMtl().recupere_mail(liste_nouveaux_contrev[len(liste_nouveaux_contrev)-1])
                ContrevenantMtl().twitter_contrevenant(liste_nouveaux_contrev[len(liste_nouveaux_contrev)-1])

                conn.commit()
        print("Base de donnee mise a jour avec succes")

    """
        Fonction qui recupere les informations d'envoie de mail
        ou sera envoye les nouveaux contrevenants
        B1
    """
    def recupere_mail(self, liste_nouveaux_contrev):
        with open('autres/couriel.yaml', 'r') as fichier:
            try:
                dict_mail = yaml.safe_load(fichier)
                destinataire = dict_mail['destinataire']
                destinateur = dict_mail['destinateur']
                password = dict_mail['password']
                ContrevenantMtl().envoie_mail(destinataire, destinateur, password, liste_nouveaux_contrev)
            except yaml.YAMLError as exc:
                print(exc)


    """
        Fonction qui se charge d'envoyer les courriels automatique
        B1
    """
    def envoie_mail(self, destinataire, destinateur, password, liste_nouveaux_contrev):
        yag = yagmail.SMTP(destinateur, password)
        yag.send(destinataire, 'nouveau contrevenant', liste_nouveaux_contrev)

    """
        Cette fonction se charge de twitter les nouveaux contrevenants
        B2
    """
    def twitter_contrevenant(self, liste_nouveaux_contrev):
        with open('autres/twitter.yaml', 'r') as fichier:
            try:
                dict_twit = yaml.safe_load(fichier)
                cle_consommateur = dict_twit['cle_consommateur']
                secret_consommateur = dict_twit['secret_consommateur']
                jeton_acces = dict_twit['jeton_acces']
                secret_acces = dict_twit['secret_acces']

                auth = tweepy.OAuthHandler(cle_consommateur, secret_consommateur)
                auth.set_access_token(jeton_acces, secret_acces)

                #Creation de l'objet api
                api = tweepy.API(auth, wait_on_rate_limit=True,
                    wait_on_rate_limit_notify=True)

                api.update_status(liste_nouveaux_contrev)
            except tweepy.TweepError as error:
                print(error)