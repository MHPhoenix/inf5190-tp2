utilisateur_insert_schema = {
    'type': 'object',
    'required': ['nom', 'courriel', 'etablissements', 'passp'],
    'properties': {
        'nom': {
            'type': 'string'
        },
        'courriel': {
            'type': 'string'
        },
        'etablissements': {
            'type': 'string'
        },
        'passp': {
            'type': 'string'
        }
    },
    'additionalProperties': False
}