import bs4
from bs4 import BeautifulSoup
import urllib.request
import sqlite3
import datetime
from traducteurMois import traducteur_mois

url = "https://storage.googleapis.com/dx-montreal/resources/92719d9b-8bf2-4dfd-b8e0-1021ffcaee2f/inspection-aliments-contrevenants-fev21.xml?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Expires=60&X-Amz-Credential=GOOG1EM55P356HYDSB5BC4M4SBSA6Q7NQVVTNVVFSZOBSOVIVW5ZADICRGUKA%2F20210311%2Feurope-west1%2Fs3%2Faws4_request&X-Amz-SignedHeaders=host&X-Amz-Date=20210311T101231Z&X-Amz-Signature=5e2c7901d2a417230dc2b1a7fbebd6a9389495cf057b9545cf2b13c007db86fd"
reponse = urllib.request.urlopen(url)
xml = BeautifulSoup(reponse, 'xml')

connection = sqlite3.connect('database/db.db')
cursor = connection.cursor()

print("Insertion dans la base de donnees en cours...")

for ctrvnant in xml.findAll('contrevenant'):

    proprietaire = ctrvnant.find('proprietaire')
    categorie = ctrvnant.find('categorie')
    etablissement = ctrvnant.find('etablissement')
    adresse = ctrvnant.find('adresse')
    ville = ctrvnant.find('ville')
    descrip = ctrvnant.find('description')
    date_infraction = ctrvnant.find('date_infraction')
    date_jugement = ctrvnant.find('date_jugement')
    montant = ctrvnant.find('montant')

    """
        Convertion des dates en ISO 8601
    """
    date_infraction = traducteur_mois(date_infraction.text)
    date_jugement = traducteur_mois(date_jugement.text)

    """
        Le Select ici permet de pourvoir recuperer un fetchone
        afin de verifier plus tard si cette contravention
        existe deja dans la base de donnees.
    """
    cursor.execute(('''SELECT * 
                    FROM contrevenant
                    WHERE proprietaire=? AND categorie=? AND etablissement=? AND 
                        adresse=? AND ville=? AND descrip=? AND date_infraction=?
                        AND date_jugement=? AND montant=?'''),
                        (proprietaire.text, categorie.text, etablissement.text, 
                        adresse.text, ville.text, descrip.text, date_infraction.date(), 
                        date_jugement.date(), montant.text))

    table = cursor.fetchone()

    """
        Si le contrevenant n'existe pas deja dans la table,
        On peut faire une insertion
    """
    if not table:
 
        cursor.execute('''INSERT INTO contrevenant(proprietaire, categorie, etablissement, 
        adresse, ville, descrip, date_infraction, date_jugement, montant)
        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)''',
        (proprietaire.text, categorie.text, etablissement.text, 
        adresse.text, ville.text, descrip.text, date_infraction.date(), date_jugement.date(), montant.text))

        connection.commit()

print("Donnees inserees avec succes")

"""Fermeture de la base de donnee"""
connection.close()