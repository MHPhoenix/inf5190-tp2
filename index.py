from flask import Flask
from flask import render_template
from flask import make_response
from flask import redirect
from flask import g
from flask import abort
from flask import request
from apscheduler.schedulers.background import BackgroundScheduler
from flask_restful import Resource
from flask_restful import Api
from dicttoxml import dicttoxml
from flask import jsonify
from flask_json_schema import JsonSchema
from flask_json_schema import JsonValidationError
import os
import csv
import json

from data import Contrevenant
from dataMtl import ContrevenantMtl
from dataUser import Utilisateur
from user import User
from schemas import utilisateur_insert_schema

"""mon application web (app)"""
app = Flask(__name__)
schema = JsonSchema(app)

"""api REST"""
api = Api (app)

"""Quand le make sera lance, la base de donnees sera automatiquement remplie"""
os.system('python3 scraper.py')

"""
    Fonctions A3
    La condition va empecher apscheduler de s'executer 2 fois
    lorsque app est en mode debug
"""
if not app.debug == 'true':
    scheduler = BackgroundScheduler()
    scheduler.add_job(ContrevenantMtl().synchro_database, 'cron', hour=00)#'interval', seconds=60
    scheduler.start()

"""
    Les 2 premieres methodes qui suivent viennent des exemples
        de classe du professeur Jacques Berger
"""

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        g._database = Contrevenant()
    return g._database

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.disconnect()

"""
    Base sur exemple de Monsieur Jacques Berger (professeur a l'uqam)
    Retourne cette erreur, quand le json schema est invalide
"""
@app.errorhandler(JsonValidationError)
def validation_error(e):
    errors = [validation_error.message for validation_error in e.errors]
    return jsonify({'error': e.message, 'errors': errors}), 400

"""
    Page d'acceuil
    Fonction qui permet aussi d'afficher tous les noms des contrevenants
    de la base de donnees dans une liste deroulante (A2)
"""
@app.route('/', methods=['GET'])
def index():
    etablissement=get_db().recherche_par_nom()
    return render_template('pages/index.html', etablissement=etablissement)

"""
    Page pour retrouver les contrevenants a partir de la barre de recherche
"""
@app.route('/resultat/', methods=['POST'])
def recherche_contrevenant():
    la_recherche = request.form['resultat']
    cherch_contrevenant = get_db().recherche(la_recherche)
    return render_template('contrevenants/resultat.html', cherch_contrevenant=cherch_contrevenant)

"""
    page pour retrouver les contrevenants entre deux dates
    Service REST(A4).
    A5
"""
@app.route('/api/contrevenants/', methods=['GET'])
def contrevenant_entre_date_api():
    #parametre de recherche(dateDebut = du, dateFin = au)
    parametre = request.args
    du = parametre.get('du')
    au = parametre.get('au')
    recherche = get_db().recherche_par_etab(du, au)
    return jsonify({'result':render_template('contrevenants/reponse.html', recherche=recherche)})


"""
    La fonction affiche l'information 
    des différentes infractions du restaurant
    A6
"""
@app.route('/api/infos/', methods=['GET'])
def info_sur_contrav():
    parametre = request.args
    nom = parametre.get('nom')
    info = get_db().info_contravention(nom)
    return jsonify({'resultat': render_template('contrevenants/infoResto.html', info=info)})

"""
    Fonction qui retourne la liste de tous les contrevenants,
    avec le nombre d'infractions connues et les retourne
    selon le nombre d'infractions(ordre decroissant)
    C1
"""
@app.route('/api/liste/', methods=['GET'])
def select_etab_decr():
    les_contrevenant = get_db().select_etab_decr()
    return jsonify(les_contrevenant)

"""
    Fonction qui retourne la liste de tous les contrevenants,
    avec le nombre d'infractions connues et les retourne
    selon le nombre d'infractions(ordre decroissant)
    en format xml
    C2
"""
@app.route('/api/listeXml/', methods=['GET'])
def select_etab_decr_xml():
    les_contrevenant = get_db().select_etab_decr()
    xml = dicttoxml(les_contrevenant, custom_root='Contrevenants',  attr_type=False)
    reponse = make_response(xml)
    reponse.headers['Content-Type'] = 'application/xml'
    return reponse

"""
    Fonction qui retourne la liste de tous les contrevenants,
    avec le nombre d'infractions connues et les retourne
    selon le nombre d'infractions(ordre decroissant)
    en format csv
    C3
"""
@app.route('/api/listeCsv/', methods=['GET'])
def select_etab_decr_csv():
    les_contrevenant = get_db().select_etab_decr()
    with open('templates/contrevenants/liste.csv', 'w') as fichier_csv:
        writer = csv.writer(fichier_csv)
        for contrev in les_contrevenant:
            writer.writerows(contrev.items())
    return render_template('contrevenants/liste.csv')

"""
    Affiche le formulaire pour creer le profil etudiant
"""
@app.route('/utilisateur/', methods=['GET'])
def afficher_formulaire():
    return render_template('contrevenants/formulaireUser.html')

"""
    Permet a l'utilisateur de creer un profil
    E1
"""
@app.route('/api/utilisateur/confirmation/', methods=['POST'])
@schema.validate(utilisateur_insert_schema)
def creer_utilisateur():
    data = request.get_json()
    user = User(None, data["nom"], data["courriel"], data["etablissements"], data["passp"])
    user = Utilisateur().creer_profil(user)
    return jsonify(user.asDictionary()), 201

"""
    Documentation RAML
"""
@app.route('/doc/')
def documentation():
    return render_template('doc.html')

"""
    Si la page demande n'existe pas
"""
@app.errorhandler(404)
def error404(error):
    return render_template('erreurs/404.html'), 404

"""
    Demarrage de l'application
"""
if __name__ == '__main__':
    app.run(debug=False, port=5000)