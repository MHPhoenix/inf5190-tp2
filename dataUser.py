import datetime
import sqlite3


"""
    S'occupe de la base de donnees du profil utilisateur
"""
class Utilisateur():

    """
        Les methodes qui suivent s'inspirent des exemples
        de classe du professeur Jacques Berger
    """
    
    def __init__(self):
        self.connection = None

    def get_connection(self):
        if self.connection is None:
            self.connection = sqlite3.connect('database/utilisateur.db')
        return self.connection

    def disconnect(self):
        if self.connection is not None:
            self.connection.close()

    """
        Insert les informations du nouvel utilisateur dans la base de donnees
    """
    def creer_profil(self, user):
        cursor = self.get_connection().cursor()
        conn = self.get_connection()
        if user.id is None:
            cursor.execute(("INSERT INTO utilisateur(nom, courriel, etablissements, pass)"
                                " VALUES(?, ?, ?, ?)"),
                                (user.nom, user.courriel, user.etablissements, user.passp))
            conn.commit()
        return user